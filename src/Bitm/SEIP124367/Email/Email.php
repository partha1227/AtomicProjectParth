<?php
namespace App\Bitm\SEIP124367\Email;

class Email{
    public $id="";
    public $emailmodel="";
    public $conn;
    public $deleted_at;

    public function prepare($data=""){
        if(array_key_exists("emailmodel",$data)){
            $this->emailmodel= $data['emailmodel'];
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;

    }

    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb21") or die("Database connection failed");
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`email` (`emailmodel`) VALUES ('".$this->emailmodel. "')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function index(){
        $_allEmail= array();
        $query="SELECT * FROM `email` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allEmail[]=$row;
        }

        return $_allEmail;
    }


    public function view(){
        $query="SELECT * FROM `email` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `atomicprojectb21`.`email` SET `emailmodel` = '".$this->emailmodel."' WHERE `email`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }


    ////////////////start line on 15.06.2016

    public function delete(){
        $query="DELETE FROM `atomicprojectb21`.`email` WHERE `email`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
//////////trash temporary
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb21`.`email` SET `deleted_at` = '".$this->deleted_at."' WHERE `email`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }



    }

//////////trash temporary list
    public function trashed(){
        $_trashedEmail= array();
        $query="SELECT * FROM `email` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_trashedEmail[]=$row;
        }

        return $_trashedEmail;

    }
//////////recover single
    public function recover(){
        $query="UPDATE `atomicprojectb21`.`email` SET `deleted_at` = NULL  WHERE `email`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }



//////////recover multiple
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectb21`.`email` SET `deleted_at` = NULL  WHERE `email`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }

//////////delete multiple
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && (count($idS)>0)){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb21`.`email`  WHERE `email`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been Deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been Deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }




    ///////////////////end line on 15.06.2016
}