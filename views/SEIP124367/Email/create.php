<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
</head>
<body>

<div class="container">
    <h2>Atomic Project- Email</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Enter Email Model:</label>
            <input type="email" name="emailmodel" class="form-control" id="email" placeholder="Enter Email Address">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
