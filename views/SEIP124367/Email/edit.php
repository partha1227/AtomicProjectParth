<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP124367\Email\Email;


$email= new Email();
$singleItem= $email->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
</head>
<body>

<div class="container">
    <h2>Atomic Project- Email</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Update Emial Model:</label>
            <input type="hidden" name="id"   value="<?php echo $singleItem['id']?>">
            <input type="email" name="emailmodel" class="form-control" id="email" value="<?php echo $singleItem['emailmodel']?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
